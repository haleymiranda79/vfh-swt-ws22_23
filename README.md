# vfh-swt-ws22_23



## Aufgabensammlung für das Modul "Moderne Softwareentwicklung" (WS 2022/23)

Dieses Repository ist eine Studentarbeit von Jenny Pilz. Sie beinhaltet die Ergebnisse der Aufgaben zu 
- Metriken
- Clean Code Development
- DVCS mit GitLab
 
Die Code-Basis ist JavaScript/TypeScript

Das Projekt beschäftigt sich mit dem Aufbau einer RESTful-Web-API unter Verwendung des Node-Frameworks Express. Ziel ist es, eine gut Code- und Strukturpraxis auf des Clean-Code-Ansatzes zu erstellen.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
User Data currently as Array in the users.controller.ts - via database interface and DTO only in the next step.
- get all users: 
    ```
    curl -X GET -H "accept: application/json" \
         http://localhost:3070/api/v1/users/ 
    ```
- get user by userName, example for "username1":
    ```
    curl -X GET -H "accept: application/json" \
         http://localhost:3070/api/v1/users/username1
    ```
- create user, example for Paul Meier, meierP21: 
    ```
    curl -X POST -H "accept: application/json" \
        -d '{"firstName":"Paul","lastName":"Meier","userName":"meierP21"}' \
        http://localhost:3070/api/v1/users/ 
   ```
- update user, example for "username2", Paulchen Muckel: 
    ```
    curl -X PUT -H "accept: application/json" \
        -d '{"firstName":"Paulchen","lastName":"Muckel","userName":"username2"}' \
        http://localhost:3070/api/v1/users/username2
   ```
   or
   ```
    curl -X PATCH -H "accept: application/json" \
        -d '{"firstName":"Paulchen","lastName":"muckel","userName":"username2"}' \
        http://localhost:3070/api/v1/users/username2
   ```
- delete user by userName, example for "username3":
    ```
    curl -X GET -H "accept: application/json" \
         http://localhost:3070/api/v1/users/username3
    ```

## License
MIT

## Project status
Laufendes Studentenprojekt von Jenny Pilz im Wintersemester 2022/23
