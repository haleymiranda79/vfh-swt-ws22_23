/**
 * Main Class
 * <p>SimpleChattyBot</p>
 *
 * @since 1.0
 * @author Jenny Pilz
 * @version 1.1
 */
package org.mediafreak.chattybot;

import java.util.Scanner;
/**
 * SimpleChattyBot Main class.
 */
public final class SimpleChattyBot {
    /**
     * Input scanner.
     */
    static final Scanner SCANNER = new Scanner(System.in);
    /**
     * Output streamer.
     */
    static final ChattyPrintStream OUTPUT = new ChattyPrintStream(System.out);

    /**
     * Main method.
     * @param args commandline parameter
     */
    public static void main(final String[] args)  {

        greet("Botti", "1971");
    }
    static void greet(final String firstName, final String birthYear) {
        OUTPUT.println("Hello! My name is " + firstName + ".");
        OUTPUT.println("I was created in " + birthYear + ".");
        OUTPUT.println("Please, remind me your name.");
        remindMeYourName();
    }
    static void remindMeYourName() {
        String name = getYourName();
        OUTPUT.println("What a great name you have, " + name + "!\n");
        guessYourAge();
    }
    static String getYourName() {
        return SCANNER.nextLine();
    }
    static void guessYourAge() {
        OUTPUT.println("Let me guess your age.");
        enterYourAge();
        int remainder3;
        int remainder5;
        int remainder7;

        try {
            remainder3 = SCANNER.nextInt();
            remainder5 = SCANNER.nextInt();
            remainder7 = SCANNER.nextInt();
        } catch (Exception error) {
            OUTPUT.println("-----> Only integer are possible. "
                    + "Please try again a valid number <-----");
            enterYourAge();
            SCANNER.nextLine();
            remainder3 = SCANNER.nextInt();
            remainder5 = SCANNER.nextInt();
            remainder7 = SCANNER.nextInt();
        }
        OUTPUT.println("Your age is "
                + getAgeOfRemainders(remainder3, remainder5, remainder7)
             + "; that's a good time to start programming!\n");

        testYou();
    }

    private static void enterYourAge() {
        OUTPUT.println("Enter remainders of dividing your age by 3, 5 and 7. "
                + "Click return after each input.");
    }

    static int getAgeOfRemainders(final int remainder3,
                                  final int remainder5, final int remainder7) {
        final int prod1 = 70;
        final int prod2 = 21;
        final int prod3 = 15;
        final int mod = 105;
        return (remainder3 * prod1 + remainder5 * prod2
                + remainder7 * prod3) % mod;
    }

    static void testYou() {
        OUTPUT.println("Let's test your programming knowledge.");
        questionForTestMessage();

        while (true) {
            int answer;

            try {
                answer = SCANNER.nextInt();
            } catch (Exception error) {
                OUTPUT.println("-----> Only integer are possible. "
                        + "Please try again a valid number <-----");
                questionForTestMessage();
                SCANNER.nextLine();
                answer = SCANNER.nextInt();
            }

            if (answer == 1) {
                theEnd();
                return;
            } else {
                OUTPUT.println("Please, try again.");
            }
        }
    }

    private static void questionForTestMessage() {
        OUTPUT.println("Why do we use methods?");
        OUTPUT.println("1. To repeat a statement multiple times.");
        OUTPUT.println("2. To decompose a program into "
                + "several small subroutines.");
        OUTPUT.println("3. To determine the execution time of a program.");
        OUTPUT.println("4. To interrupt the execution of a program.");
    }

    static void theEnd() {
        OUTPUT.println("Congratulations. Have a nice day!");
    }
}
