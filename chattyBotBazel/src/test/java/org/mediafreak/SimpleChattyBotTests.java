package org.mediafreak;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class SimpleChattyBotTests {

    @Test
    @DisplayName("Age calculator should work")
    void getAgeOfRemainders() {
        int remainder3 = 2;
        int remainder5 = 2;
        int remainder7 = 5;
        int myAge = 47;
        assertEquals(myAge, org.mediafreak.SimpleChattyBot.getAgeOfRemainders(
                remainder3,
                remainder5,
                remainder7));
    }
}