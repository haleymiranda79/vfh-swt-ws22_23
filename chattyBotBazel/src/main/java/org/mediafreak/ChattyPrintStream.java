package org.mediafreak;

import java.io.PrintStream;

public class ChattyPrintStream extends PrintStream {
    /**
     * ChattyPrintStream Constructor.
     * @param out output stream parameter
     */
    public ChattyPrintStream(final PrintStream out) {
        super(out);
    }

    /**
     * Print out messages in the console.
     * @param message  The {@code String} to be printed.
     */
    @Override
    public void println(final String message) {
        System.out.println(message);
    }
}
