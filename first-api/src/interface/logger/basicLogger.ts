import { LoggingFunction } from './loggingFunction';

export interface BasicLogger {
    debug: LoggingFunction;
    error: LoggingFunction;
    info: LoggingFunction;
    log: LoggingFunction;
    warn: LoggingFunction;
}