import express from 'express';
import { ConsoleLogger } from '../../utils/logger';

const logger = new ConsoleLogger();

const bunchOfUsers = [{ firstName: 'fnam1', lastName: 'lnam1', userName: 'username1' },
  { firstName: 'puse', lastName: 'muckel', userName: 'username2' },
  { firstName: 'fnam3', lastName: 'lnam3', userName: 'username3' },
  { firstName: 'fnam4', lastName: 'lnam4', userName: 'username4' }];

export async function get(request: any, response: any, next: any) {
  try {
    logger.info(`Returns a set of users: ${JSON.stringify(bunchOfUsers)}, ${bunchOfUsers.length} user objects`);  
    response.json(bunchOfUsers);
  } catch (error) {
    logger.error(`Error while getting users`, error.message);
    next(error);
  }
}

export async function getUserByUserName(request: any, response: any, next: any) {
  try {
    logger.info(`filter users by username:::::${request.params.userName}`);
    const filterUsersByUserName = bunchOfUsers.filter(usr => request.params.userName === usr.userName);
    response.json(filterUsersByUserName);
  } catch (error) {
    logger.error(`Error while getting users`, error.message);
    next(error);
  }
}

export async function create(request: any, response: any, next: any) {
  try {
    bunchOfUsers.push(request.body);
    logger.info(`Added user object: ${bunchOfUsers}, ${bunchOfUsers.length}`);  
    response.json(bunchOfUsers);
  } catch (error) {
    console.error(`Error while creating user`, error.message);
    next(error);
  }
}

export async function update(request: any, response: any, next: any) {
  try {
    //response.json(await bunchOfUsers.update(request.params.id, request.body));
  } catch (error) {
    console.error(`Error while updating user`, error.message);
    next(error);
  }
}

export async function remove(request: any, response: any, next: any) {
  try {
    const deleteUserID = bunchOfUsers.findIndex(usr => request.params.userName === usr.userName);
    logger.info(`find users index by username:::::${request.params.userName} - ${deleteUserID}`);
    bunchOfUsers.splice(deleteUserID,1);
    response.json(bunchOfUsers);
  } catch (error) {
    console.error(`Error while deleting user`, error.message);
    next(error);
  }
}