import express from 'express';
import { get, getUserByUserName, create, update, remove } from '../users/users.controller';

const usersRouter = express.Router();

usersRouter.get('/', get);
usersRouter.get('/:userName', getUserByUserName);
  
usersRouter.post('/', create);

usersRouter.put('/:userName', update);

usersRouter.patch('/:userName', update);

usersRouter.delete('/:userName', remove);

export default usersRouter;